const MongoClient = require('mongodb').MongoClient
const ObjectID = require('mongodb').ObjectID

const swal = require('sweetalert2')

const app = angular.module('scrum-manager', ['ui.router', 'oc.lazyLoad'])

app.config($stateProvider => {
  $stateProvider
    .state({ name: 'login', views: { '@': { templateUrl: 'components/login/login.html', controller: 'login' } }, resolve: { controlador: function($ocLazyLoad) { return $ocLazyLoad.load(['components/login/login.js', 'components/login/login.css']); } } })
    .state({ name: 'home', views: { '@': { templateUrl: 'components/sistema/sistema.html' }, 'content@home': { templateUrl: 'components/home/home.html', controller: 'home' }, 'nav@home': { templateUrl: 'components/nav/nav.html', controller: 'nav' } }, resolve: { controlador: function($ocLazyLoad) { return $ocLazyLoad.load(['components/home/home.js', 'components/home/home.css', 'components/nav/nav.js', 'components/nav/nav.css']); } } })
    .state({ name: 'home.miPerfil', views: { 'content@home': { templateUrl: 'components/miPerfil/miPerfil.html', controller: 'miPerfil' } }, resolve: { controlador: function($ocLazyLoad) { return $ocLazyLoad.load('components/miPerfil/miPerfil.js'); } } })
    .state({
      name: 'home.crearNota',
      views: {
        'content@home': {
          templateUrl: 'components/crearNota/crearNota.html',
          controller: 'crearNota'
        }
      },
      resolve: {
        controlador: function($ocLazyLoad) {
          return $ocLazyLoad.load('components/crearNota/crearNota.js');
        }
      }
    })
    .state({
      name: 'home.misNotas',
      views: {
        'content@home': {
          templateUrl: 'components/misNotas/misNotas.html',
          controller: 'misNotas'
        }
      },
      resolve: {
        controlador: function($ocLazyLoad) {
          return $ocLazyLoad.load('components/misNotas/misNotas.js');
        }
      }
    })
})

app.run(async ($state, $rootScope) => {
  if (localStorage.getItem("usuario_conectado")) {
    let client = await MongoClient('mongodb://localhost:27017/scrum-manager', {
      useNewUrlParser: true
    }).connect()
    let usuario = await client.db().collection('usuarios').findOne(ObjectID(localStorage.getItem("usuario_conectado")).valueOf())
    if (usuario) {
      $rootScope.usuario_conectado = usuario
      $state.go('home')
    } else {
      localStorage.removeItem("usuario_conectado")
      $state.go('login')
    }
  } else $state.go('login')
})
