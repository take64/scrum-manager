angular.module('scrum-manager').controller('login', function($scope, $rootScope, $state) {
  $scope.usuario = {
    nombre: '',
    correo: ''
  }
  $scope.login = async function() {
    let client = await MongoClient('mongodb://localhost:27017/scrum-manager', { useNewUrlParser: true }).connect()
    let usuario = await client.db().collection('usuarios').findOne($scope.usuario)
    if (!usuario) {
      let result = await client.db().collection('usuarios').insertOne($scope.usuario)
      usuario = await client.db().collection('usuarios').findOne(result.insertedId)
    }
    $rootScope.usuario_conectado = usuario;
    localStorage.setItem("usuario_conectado", usuario._id.toString())
    await client.close()
    $state.go('home')
  }
})
