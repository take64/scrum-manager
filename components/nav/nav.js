angular.module('scrum-manager').controller('nav', function($scope, $rootScope, $state) {
  $scope.cerrar_sesion = () => {
    $rootScope.usuario_conectado = null
    localStorage.removeItem("usuario_conectado")
    $state.go('login')
  }
  $scope.current_active = linkName => $state.current.name == linkName
})
