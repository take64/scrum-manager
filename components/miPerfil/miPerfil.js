angular.module('scrum-manager').controller('miPerfil', function($scope, $rootScope, $state, $timeout) {
  $scope.editar_perfil = async () => {
    try {
      $scope.editando = true
      let client = await MongoClient('mongodb://localhost:27017/scrum-manager', { useNewUrlParser: true }).connect()
      let update = {}
      for (let key in $rootScope.usuario_conectado)
        if (key != '_id' && key != 'nombre') update[key] = $rootScope.usuario_conectado[key];
      let result = await client.db().collection('usuarios').updateOne({ _id: $rootScope.usuario_conectado._id }, { $set: update })
      await client.close()
      if(result.modifiedCount) swal('Se ha modificado la información de su perfil', '', 'success')
      else swal('No se han realizado cambios', 'Debe aplicar un cambio al menos', 'warning')
    } catch (e) {
      swal(e.name, e.message, 'error')
    } finally {
      $timeout(() => $scope.editando = false, 0)
    }
  }
})
