angular.module('scrum-manager').controller('misNotas', function($scope, $rootScope, $timeout) {
  $scope.notas = []

  $scope.cargar_notas = async () => {
    let mongoClient = await MongoClient('mongodb://localhost:27017/scrum-manager', { useNewUrlParser: true }).connect()
    let result = await mongoClient.db().collection('notas').find({ creador: $rootScope.usuario_conectado._id }).toArray()
    $timeout(() => $scope.notas = result, 0)
    await mongoClient.close()
  }

  $scope.cargar_notas()
})
