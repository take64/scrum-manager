angular.module('scrum-manager').controller('crearNota', function($scope, $rootScope, $state, $timeout) {
  $scope.nota = {
    nombre: '',
    texto: '',
    creador: $rootScope.usuario_conectado._id
  }
  $scope.crear_nota = async () => {
    try {
      $scope.creando = true
      $scope.nota.creacion = new Date()
      let client = await MongoClient('mongodb://localhost:27017/scrum-manager', { useNewUrlParser: true }).connect()
      let result = await client.db().collection('notas').insertOne($scope.nota)
      await client.close()
      if(result.insertedCount) swal('Se ha ingresado la nota al sistema', 'Ahora se dirigo al módulo "Mis notas"', 'success').then(() => $state.go('home.misNotas'))
      else swal('La nota NO ha sido ingresada al sistema', 'Favor revise los caractéres ingresados', 'warning')
    } catch (e) {
      swal(e.name, e.message, 'error')
    } finally {
      $timeout(() => $scope.creando = false, 0)
    }
  }
})
